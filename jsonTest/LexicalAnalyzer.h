/*
 * LexicalAnalyzer.h
 *
 *  Created on: Feb 16, 2015
 *      Author: uis
 */

#ifndef LEXICALANALYZER_H_
#define LEXICALANALYZER_H_

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "ICharSource.h"

#define IS_CHARACTER(x)(((x>='a')&&(x<='z'))||((x>='A')&&(x<='Z'))||(x=='_'))
#define IS_NUMERIC(x)(x>='0')&&(x<='9')
#define IS_SPACE(x)(x==' ')||(x=='\r')||(x=='\n')||(x=='\t')
#define IS_ALPHANUMERIC(x)(((x>='a')&&(x<='z'))||((x>='A')&&(x<='Z'))||(x=='_')||((x>='0')&&(x<='9')))

#define EQUALS(A,B) (strcmp((char *)A,(char *)B) == 0)


namespace uis {

	enum TokenType{
		TOKEN_ERROR, // 0
		TOKEN_OPEN_BRACE, // {
		TOKEN_CLOSE_BRACE, // }
		TOKEN_OPEN_BRACKET, // [
		TOKEN_CLOSE_BRACKET, // ]
		TOKEN_COLON, // :
		TOKEN_COMMA, // ,
		TOKEN_WORD
	};

	enum TokenSubType{
		TYPE_NULL,
		TYPE_INT,
		TYPE_DOUBLE,
		TYPE_STRING,
		TYPE_STRING_IDENTIFIER
	};

	//TODO: check if this string can be used as identifier
	struct Token{
		enum TokenType type;
		enum TokenSubType varType;
//		int intValue; // variavel convertida para inteiro
//		double doubleValue; // variavel convertida para duble;
		char * stringValue; // representacao literal do token, ignorando aspas
	};

	class LexicalAnalyzer {
		private:

			int maxVarNameSize;
			int maxStringSize;
			struct Token * token;

		public:

			struct Token * Token;
			ICharSource * CharSource;

			LexicalAnalyzer(int maxVarNameSize, int maxStringSize);

			struct Token * next();

			virtual ~LexicalAnalyzer();

		};
	}

#endif /* LEXICALANALYZER_H_ */
