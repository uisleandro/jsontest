/*
 * MyWebApp.cpp
 *
 *  Created on: Feb 23, 2015
 *      Author: uis
 */

#include "MyWebApp.h"
#include <iostream>
#include "TCPConnection.h"

#define EQUALS(A,B) (strcmp((char *)A,(char *)B) == 0)

namespace uis {

MyWebApp::MyWebApp() {
	// TODO Auto-generated constructor stub

}

MyWebApp::~MyWebApp() {
	// TODO Auto-generated destructor stub
}

void MyWebApp::exec(){

	TCPConnection * conn = new TCPConnection("localhost", 80);

	conn->Open();

	conn->println("GET /hello.json HTTP/1.1");
	conn->println("Host: localhost");
	conn->println("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn->println("Accept-Language: en-US,en;q=0.5");
	conn->println("Connection: keep-alive");
	conn->println("\r\n");

	char * header = new char[1024];
	conn->readln(header);

	int len = 0;
	while(!EQUALS(header,"\0")){
		if(strstr(header, "Content-Length") != nullptr){
				char * content2 = strchr(header, ':');
				content2+=2;
				len = atoi(content2);
		}
		std::cout << header << std::endl;
		conn->readln(header);
	}
	delete header;

	int bufferSize = 10;
	int n = len / 10;
	int left = len % 10;

	char * body = new char[bufferSize];
	int i = 0;

	//TODO: substituir while por if
	while(i < n){
		//TODO: dividir de 10 em 10
		conn->receive(bufferSize, body);
		body[bufferSize] = '\0';

		//mostra a linha
		std::cout << body << std::endl;

		//processar os strings


		i++;
	}
	if(left > 0){
		conn->receive(left, body);
		body[left] = '\0';
		std::cout << body << std::endl;

		left = 0;
	}
	//esta é uma situação de
	//produtor-consumidor

	delete body;
	delete conn;
}

} /* namespace uis */
