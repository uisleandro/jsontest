#include <iostream>
#include <string>
#include <memory>

#include <string.h>

#include "Parser.h"
#include "CharacterSource.h"
#include "LexicalAnalyzer.h"
#include "WebStringSource.h"
#include "Collection.h"

std::string getTokenName(int t){

	switch(t){
	case 0: return "TOKEN_ERROR";
	case 1: return "TOKEN_OPEN_BRACE";
	case 2: return "TOKEN_CLOSE_BRACE";
	case 3: return "TOKEN_OPEN_BRACKET";
	case 4: return "TOKEN_CLOSE_BRACKET";
	case 5: return "TOKEN_COLON";
	case 6: return "TOKEN_COMMA";
	case 7: return "TOKEN_VALUE_INT";
	case 8: return "TOKEN_VALUE_DOUBLE";
	case 9: return "TOKEN_VALUE_STRING";
	case 10: return "TOKEN_VALUE_STRING_DOUBLE_QUOTATION";
	case 11: return "TOKEN_VALUE_STRING_SINGLE_QUOTATION";
	case 12: return "TOKEN_STRING_IDENTIFIER";
	default:
		return "UNKNOW";
	}

}

using namespace std;
using namespace uis;

int main(int argc, char ** argv){

	uis::Parser * parser = new uis::Parser();
	uis::CollectionFactory * a = parser->Parse();
	std::cout << a->toString() << std::endl;

	/*
	for(std::deque<CollectionFactory *>::iterator it1 = a->CollectionArray->begin();
			it1!= a->CollectionArray->end(); ++it1){

		std::cout << "\t" << (*it1)->toString() << std::endl;

		for(std::unordered_map<std::string,CollectionFactory *>::iterator it2 = (*it1)->AssociativeArray->begin();
					it2!= (*it1)->AssociativeArray->end(); ++it2){
				std::cout << "\t\t" << (*it2).first;

				if((*it2).second->Type == CHAR_PTR){
					std::cout << " = " << (*it2).second->StringValue << std::endl;
				}
		}
	}
	*/

}
