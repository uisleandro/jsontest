#include "Collection.h"

namespace uis {

CollectionFactory::CollectionFactory(CollectionItemType type) {

	this->Type = type;

	switch (type) {

	case DEQUE:

		this->CollectionArray = new std::deque<CollectionFactory *>();

		break;
	case UNORDERED_MAP:

		this->AssociativeArray =
				new std::unordered_map<std::string, CollectionFactory *>();

		break;
	case CHAR_PTR:

		this->StringValue = nullptr;

		break;

	}

}

CollectionFactory::~CollectionFactory() {

}

std::string CollectionFactory::toString() {

	std::string s;
	bool first = true;

	switch (Type) {

	case DEQUE:

		s += "[";

		for(std::deque<CollectionFactory *>::iterator it1 = CollectionArray->begin();
				it1!= CollectionArray->end(); ++it1){

			if(first){
				first = false;
			}else{
				s+= ",";
			}

			s += (*it1)->toString();
		}

		s += "]";

		return s;

		break;
	case UNORDERED_MAP:

		s += "{";

		for(std::unordered_map<std::string,CollectionFactory *>::iterator it2 = AssociativeArray->begin();
							it2!= AssociativeArray->end(); ++it2){
			if(first){
				first = false;
			}else{
				s+= ",";
			}

			s += "\""+it2->first+"\"";
			s += ":";
			s += it2->second->toString();

		}

		s += "}";

		return s;

		break;
	case CHAR_PTR:

		s = StringValue;

		return "\""+ s +"\"";

		break;

	default:

	 return "";

	break;

	}



}

}
