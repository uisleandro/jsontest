/*
 * WebStringSource.cpp
 *
 *  Created on: Feb 24, 2015
 *      Author: uis
 */

#include "WebStringSource.h"

namespace uis {

WebStringSource::WebStringSource(int bufferSize, const char * ip, int portno) {
	this->bufferSize = bufferSize;

	//the header is not a null pointer
	this->Buffer = new char[bufferSize];
	this->conn = new TCPConnection(ip, portno);

	this->header = nullptr;
	this->thisPart = 0;
	this->parts = 0;
	this->left = 0;
}

WebStringSource::~WebStringSource() {

	if(this->header != nullptr){
		delete this->header;
	}

	delete this->Buffer;
	delete this->conn;
}

bool WebStringSource::Get(const char * host, const char * path){

	if(!conn->Open()) return false;

	this->header = nullptr;
	this->thisPart = 0;
	this->parts = 0;
	this->left = 0;

	char * msg = new char[512];
	strcpy(msg,"GET ");
	strcat(msg, path);
	strcat(msg, " HTTP/1.1");
	conn->println(msg);

	strcpy(msg,"Host: ");
	strcat(msg, host);
	conn->println(msg);

	conn->println("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn->println("Accept-Language: en-US,en;q=0.5");
	conn->println("Connection: keep-alive");
	conn->println("\r\n");

	return true;
}

bool WebStringSource::next(){

		//Process the header
		if(this->header == nullptr){
			this->header = new char[1024];
			conn->readln(this->header);
			int len = 0;
			while(!EQUALS(this->header,"\0")){
				if(strstr(this->header, "Content-Length") != nullptr){
						char * content2 = strchr(this->header, ':');
						content2+=2;
						len = atoi(content2);
				}
//				std::cout << this->header << std::endl;
				conn->readln(this->header);
			}
			this->parts = len / this->bufferSize;
			this->left = len % this->bufferSize;
		}

		//Process the body
		if(this->thisPart < parts){
			conn->receive(bufferSize, this->Buffer);
			this->Buffer[this->bufferSize] = '\0';

			//show a line
//			std::cout << this->Buffer << std::endl;

			this->thisPart++;

			return true;
		}
		if(this->left > 0){
			conn->receive(this->left, this->Buffer);
			this->Buffer[this->left] = '\0';
			//std::cout << this->Buffer << std::endl;
			this->left = 0;

			return true;
		}

	return false;
}

} /* namespace uis */
