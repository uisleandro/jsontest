/*
 * LexicalAnalyzer.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: uis
 */

#include "LexicalAnalyzer.h"

namespace uis {

	//TODO: seems good if you do not convert the numbers,
	//this way you can save everything on
	//a database as it comes
	LexicalAnalyzer::LexicalAnalyzer(int maxVarNameSize, int maxStringSize) {

		this->maxVarNameSize = maxVarNameSize;
		this->maxStringSize = maxStringSize;
		this->CharSource = nullptr;

		//reutilizacao do token?
		this->Token = nullptr;
		this->token = new struct Token;
		this->token->stringValue = nullptr;

		/*
		 * uma variavel Toke é usada porque é uma variavel anulavel
		 * */

	}

	//TODO: vou precisar de token nulo
	struct Token * LexicalAnalyzer::next() {

		if(this->CharSource == nullptr){
			std::cerr << "LexicalAnalyzer::getToken() -> this->CharSource is null";
			exit(1);
		}

		char * lexema;
		int lexema_index = 0;
		int state = 0;

		//TODO: if those conditions are true
		//a "string" can be used as an identifier
		bool isFirstChar = true;
		bool isFirsCharLetter = false;
		bool isEveryCharAlpha = false;

		bool keepRunning = true;

		//fills ThisChar and NextChar
		if(this->CharSource->ThisChar == '\0'){
			keepRunning = this->CharSource->next();
		}

		while(keepRunning){

			switch (state) {

			case 0:
				if (IS_SPACE(this->CharSource->ThisChar)) {
					//consume white spaces
					keepRunning = this->CharSource->next();
					continue;
				}
				if (this->CharSource->ThisChar == '{') {
					// token {
					this->token->type = TOKEN_OPEN_BRACE;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == '}') {
					// token }
					this->token->type = TOKEN_CLOSE_BRACE;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == '[') {
					// token [
					this->token->type = TOKEN_OPEN_BRACKET;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == ']') {
					// token ]
					this->token->type = TOKEN_CLOSE_BRACKET;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == ':') {
					// token :
					this->token->type = TOKEN_COLON;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == ',') {
					// token ,
					this->token->type = TOKEN_COMMA;
					keepRunning = this->CharSource->next();
					this->Token = this->token;
					return this->Token;
				} else if (this->CharSource->ThisChar == '"') {
					// "string" begin
					lexema = new char[this->maxStringSize];
					keepRunning = this->CharSource->next();
					state = 1;
					continue;
				} else if (this->CharSource->ThisChar == '\'') {
					// 'string' begin
					lexema = new char[this->maxStringSize];
					keepRunning = this->CharSource->next();
					state = 2;
					continue;
				} else if (this->CharSource->ThisChar == '.') {
					// float begin
					lexema = new char[this->maxVarNameSize];

					lexema[lexema_index] = '0';
					lexema_index++;
					lexema[lexema_index] = '.';
					lexema_index++;
					keepRunning = this->CharSource->next();
					state = 6;
					continue;
				} else if (IS_NUMERIC(this->CharSource->ThisChar)) {
					// number begin

					//the variable 'lexema' will be converted to an integer or double
					lexema = new char[this->maxVarNameSize];

					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					keepRunning = this->CharSource->next();
					state = 3;
					continue;

				} else if (IS_CHARACTER(this->CharSource->ThisChar)) {
					// identifier begin or string begin

					//this value will be stored
					lexema = new char[this->maxVarNameSize];

					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;

					keepRunning = this->CharSource->next();
					state = 4;
					continue;
				}else{
					this->Token = nullptr;
					return this->Token;
				}

				break;
			case 1:
				//TODO: test if this token can be used as
				// an identifier
				if(isFirstChar && IS_CHARACTER(this->CharSource->ThisChar)){
					isFirsCharLetter = true;
					isEveryCharAlpha = true;
				}
				else if((this->CharSource->ThisChar != '"') && !IS_ALPHANUMERIC(this->CharSource->ThisChar)){
					isEveryCharAlpha = false;
				}
				isFirstChar = false;


				if(this->CharSource->ThisChar == '\\'){
					// caractere especial usado para codificar string
					if(this->CharSource->NextChar == '\''){
						lexema[lexema_index] = '\'';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == '\"'){
						lexema[lexema_index] = '\"';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 'r'){
						lexema[lexema_index] = '\r';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 'n'){
						lexema[lexema_index] = '\n';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 't'){
						lexema[lexema_index] = '\t';
						lexema_index++;
					}
					// consumo de 2 caracteres
					keepRunning = this->CharSource->next();
					keepRunning = this->CharSource->next();
					continue;
				}
				else if(this->CharSource->ThisChar != '"'){
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					keepRunning = this->CharSource->next();
					continue;
				}
				else if(this->CharSource->ThisChar == '"'){
					//vai para o estado de aceitacao de string
					keepRunning = this->CharSource->next();
					state = 5;
					continue;
				}

				break;

			case 2:

				//TODO: test if this token can be used as
				// an identifier
				if(isFirstChar && IS_CHARACTER(this->CharSource->ThisChar)){
					isFirsCharLetter = true;
					isEveryCharAlpha = true;
				}
				else if((this->CharSource->ThisChar != '\'') && !IS_ALPHANUMERIC(this->CharSource->ThisChar)){
					isEveryCharAlpha = false;
				}
				isFirstChar = false;

				if(this->CharSource->ThisChar == '\\'){
					// caractere especial usado para codificar string
					if(this->CharSource->NextChar == '\''){
						lexema[lexema_index] = '\'';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == '\"'){
						lexema[lexema_index] = '\"';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 'r'){
						lexema[lexema_index] = '\r';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 'n'){
						lexema[lexema_index] = '\n';
						lexema_index++;
					}
					else if(this->CharSource->NextChar == 't'){
						lexema[lexema_index] = '\t';
						lexema_index++;
					}
					// consumo de 2 caracteres
					keepRunning = this->CharSource->next();
					keepRunning = this->CharSource->next();
					continue;
				}
				else if(this->CharSource->ThisChar != '\''){
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					keepRunning = this->CharSource->next();
					continue;
				}
				else if(this->CharSource->ThisChar == '\''){
					//vai para o estado de aceitacao de string
					keepRunning = this->CharSource->next();
					state = 5;
					continue;
				}

				break;

			case 3:
				//consome o numero: verifica se pode ser double

				if(IS_NUMERIC(this->CharSource->ThisChar)){ //pode ser string ou double

					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;

					//the status is the same
					keepRunning = this->CharSource->next();
					continue;
				}
				else if(this->CharSource->ThisChar == '.'){
					//vai para o processamento de double
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					state = 6;

					keepRunning = this->CharSource->next();
					continue;
				}
				else{
					//Do not consume the actual character
					//because it is not a number anymore
					//it is part of another token

					this->token->type = TOKEN_WORD;
					this->token->varType = TYPE_INT;
					lexema[lexema_index] = '\0';

					//not using union anymore
					/* not deleting anymore
					if(this->token->stringValue != nullptr){
						delete this->token->stringValue;
					}
					*/

					this->token->stringValue = lexema;
					this->Token = this->token;
					return this->Token;
				}

				break;

			case 4:
				//process all the identifier
				if(IS_ALPHANUMERIC(this->CharSource->ThisChar)){
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;

					//the status is the same
					keepRunning = this->CharSource->next();
					continue;
				}
				else{
					//TODO: this should have another type
					this->token->type = TOKEN_WORD;
					this->token->varType = TYPE_STRING_IDENTIFIER;

					lexema[lexema_index] = '\0';

					//not using union anymore
					/* not deleting anymore
					if(this->token->stringValue != nullptr){
						delete this->token->stringValue;
					}
					*/
					this->token->stringValue = lexema;
					return this->Token;
				}

				break;

			case 5:

				// accept as string
				// do not consume characters
				this->token->type = TOKEN_WORD;

				if(isFirsCharLetter && isEveryCharAlpha){
					this->token->varType = TYPE_STRING_IDENTIFIER;
				}
				else{
					this->token->varType = TYPE_STRING;
				}

				lexema[lexema_index] = '\0';

				//not using union anymore
				/* not deleting anymore
				if(this->token->stringValue != nullptr){
					delete this->token->stringValue;
				}
				*/
				this->token->stringValue = lexema;
				return this->Token;

				break;

			case 6:
				// start processing float: which means [0-9] + '.'
				if(IS_NUMERIC(this->CharSource->ThisChar)){ //pode ser string ou double
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					state = 7;
					keepRunning = this->CharSource->next();
					continue;
				}
				else{
					std::cerr << "character at: " << this->CharSource->Count << "a number is expected after the character '.'";
					exit(1);
				}

				break;

			case 7:
				// ends processing float
				if(IS_NUMERIC(this->CharSource->ThisChar)){
					lexema[lexema_index] = this->CharSource->ThisChar;
					lexema_index++;
					keepRunning = this->CharSource->next();
					// keep consuming numbers
					continue;
				}
				else{ //float end
					lexema[lexema_index] = '\0';
					this->token->type = TOKEN_WORD;
					this->token->varType = TYPE_DOUBLE;

					//not using union anymore
					/* not deleting anymore
					if(this->token->stringValue != nullptr){
						delete this->token->stringValue;
					}
					*/
					this->token->stringValue = lexema;
					return this->Token;
				}
				break;
			}
			this->Token = nullptr;
			return this->Token;
		}
		this->Token = nullptr;
		return this->Token;
	}

	LexicalAnalyzer::~LexicalAnalyzer()
	{
		if(this->token != nullptr)
			delete this->token;
		if(this->CharSource != nullptr)
			delete this->CharSource;
	}

}
