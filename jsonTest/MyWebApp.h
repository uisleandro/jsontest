/*
 * MyWebApp.h
 *
 *  Created on: Feb 23, 2015
 *      Author: uis
 */

#ifndef MYWEBAPP_H_
#define MYWEBAPP_H_

namespace uis {

class MyWebApp {
public:
	MyWebApp();
	virtual ~MyWebApp();

	void exec();
};

} /* namespace uis */

#endif /* MYWEBAPP_H_ */
