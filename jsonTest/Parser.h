/*
 * Parser.h
 *
 *  Created on: Feb 26, 2015
 *      Author: uis
 */

#ifndef PARSER_H_
#define PARSER_H_

#include <string>
#include "LexicalAnalyzer.h"
#include "CharacterSource.h"
#include "WebStringSource.h"
#include <iostream>
#include "Collection.h"

namespace uis {

class Parser {

private:
	bool object(CollectionFactory ** current); // {

	bool attribute(CollectionFactory ** current); // "k" : "v" (i can take all as string)
	bool array(CollectionFactory ** current); // an object array or array of arrays

	bool stringKey(std::string &current);
	bool stringValue(CollectionFactory ** current);


#ifdef DEBUG
	void debug();
#endif


//	bool intValue();
//	bool doubleValue();

protected:

public:

	LexicalAnalyzer * analex;
	CharacterSource * characterSource;
	WebStringSource * webStringSource;

	Parser();
	virtual ~Parser();

	CollectionFactory * Parse();// [ or {
};

} /* namespace uis */

#endif /* PARSER_H_ */
