/*
 * IReader.h
 *
 *  Created on: Feb 17, 2015
 *      Author: uis
 */

#ifndef ICHARSOURCE_H_
#define ICHARSOURCE_H_


/**
 * Uma classe otimizada para leitura
 * vantagens: possibilidade de ter varios tipos
 * de classe Readers para arquivos
 * ou string ou char *
 * */
namespace uis{

	class ICharSource {
		public:
			//o caractere atual
			//the working character
			char ThisChar;

			//o proximo caracter
			//the next working character
			char NextChar;

			int Count;

			//A must implement function
			//append
			virtual void append(const char * &s) = 0;

			//undefined destructor
			virtual ~ICharSource(){};

			//returns true if the
			//next character is avaliable
			virtual bool next() = 0;
	};

}

#endif /* ICHARSOURCE_H_ */
