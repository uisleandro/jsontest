/*
 * CharacterSource.cpp
 *
 *  Created on: Feb 17, 2015
 *      Author: uis
 */

#include "CharacterSource.h"

#include <iostream>

namespace uis {

//TODO: em c++ a unica forma de uma funcao
//modificar outra é atraves de classes
//em que uma funcao pode "fabricar" outra
//void nullFn(){}

CharacterSource::CharacterSource(int bufferSize) {
	this->bufferSize = bufferSize;
	this->buffer = 0;
	this->position = 0;
	this->length = 0;
	this->StringSource = nullptr;
	this->ThisChar = '\0';
	this->NextChar = '\0';
	this->Count = 0;
}

CharacterSource::~CharacterSource() {

	if(this->buffer != nullptr){
		if(this->StringSource == nullptr){
			delete this->buffer;
		}
		else{
			if(this->buffer != this->StringSource->Buffer){
				delete this->buffer;
			}
			delete this->StringSource;
		}
	}

}

/**
 * TODO: the function append() can cause problems
 * i'm not sure about it
 * */
void CharacterSource::append(const char * &s){

	int len = strlen(s);

	if (this->buffer == nullptr) {
		this->position = 0;
		this->buffer = new char[len];
		strcpy(this->buffer, s);
		this->length = strlen(this->buffer);
	} else {

		int count = (this->length - this->position);
		len += count;

		char * t = new char[len];
		t[0]='\0';

		if(count > 0){
			strcpy(t, &this->buffer[this->position]);
			t[count]='\0';
		}

		//concatenation
		strcat(t, s);

		// free memory only if it belongs to this class
		if( (this->buffer != nullptr) &&
			(this->StringSource != nullptr) &&
			(this->buffer != this->StringSource->Buffer) ){
			delete this->buffer;
		}

		this->buffer = t;
		this->length = strlen(t);
		this->position = 0;
	}
}

//modified to use string source
bool CharacterSource::next(){

	//if(this->ThisChar != '\0') std::cout << this->ThisChar;

	if(this->position < this->length){
		this->ThisChar = this->NextChar;
		this->NextChar = this->buffer[this->position++];
		this->Count++;
		return true;
	}
	else{
		// solve the next char first
		// also fix the buffer
		if(this->NextChar == '\0'){

			//at this point it is already possible load new characters
			if( (this->StringSource != nullptr) &&
			(this->StringSource->next()) ){

				//std::cout << std::endl;

				//set the buffer
				if(this->buffer != this->StringSource->Buffer){
					//free the memory if it belongs only
					//to the current class
					if(this->buffer != nullptr){
						delete this->buffer;
					}

					//set the new Buffer (do not append string)
					this->buffer = this->StringSource->Buffer;
				}

				//set the length
				this->length = strlen(this->buffer);

				this->position = 0;
				this->ThisChar = this->buffer[this->position++];
				this->NextChar = this->buffer[this->position++];
				this->Count++;
				return true;
			} // when new characters can be read from the string source

		}
		else{

			this->ThisChar = this->NextChar;
			this->NextChar = '\0';

			//at this point it is already possible load new characters
			if( (this->StringSource != nullptr) &&
			(this->StringSource->next()) ){

				//std::cout << std::endl;

				//set the buffer
				if(this->buffer != this->StringSource->Buffer){
					//free the memory if it belongs only
					//to the current class
					if(this->buffer != nullptr){
						delete this->buffer;
					}

					//set the new Buffer (do not append string)
					this->buffer = this->StringSource->Buffer;
				}

				//set the length
				this->length = strlen(this->buffer);
				this->position = 0;
				this->NextChar = this->buffer[this->position++];

			} // when new characters can be read from the string source
			this->Count++;
			return true;
		}

	} // when finish reading all characters
	return false;
}

} /* namespace uis */
