/*
 * TCPConection.h
 *
 *  Created on: Feb 23, 2015
 *      Author: Uisleandro
 */

#ifndef TCPCONECTION_H_
#define TCPCONECTION_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>

namespace uis {

class TCPConnection {

private:

	const char * address;

	int sockfd, portno;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	int length;

	void bind();

public:
	TCPConnection();
	TCPConnection(const char * ip, int portno);
	virtual ~TCPConnection();

	bool Open();
	void Close();

	void setLength(int len);
	int send(std::string &data);
	int send(const char * data);
	int send(int length, char * &data);
	int receive(int length, char * &data);

	void println(const char * line);
	void readln(char * &line);

};

} /* namespace std */

#endif /* CONECTION_H_ */

