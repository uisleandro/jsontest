/*
 * TCPConnection.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: uisleandro
 */

#include "TCPConnection.h"


namespace uis {

TCPConnection::TCPConnection() {
	this->address = "127.0.0.1";
	this->portno = 80;
	this->length = 1024;
	this->server = nullptr;
	this->sockfd = 0;
}


TCPConnection::TCPConnection(const char * ip, int portno) {
	this->address = ip;
	this->portno = portno;
	this->length = 1024;
	this->server = nullptr;
	this->sockfd = 0;
	#ifdef SHOW_DEBUG_MESSAGES
		std::cout << "TCPConnection(" << ip << "," << portno << ")" << std::endl;
	#endif
}

TCPConnection::~TCPConnection() {

}

bool TCPConnection::Open(){

	    this->sockfd = socket(AF_INET, SOCK_STREAM, 0);

	    if (this->sockfd < 0){
			#ifdef SHOW_ERROR_MESSAGES
	    		std::cerr << "ERROR opening socket" << std::endl;
			#endif
	        return false;
	    }

	    this->server = gethostbyname(this->address);
	    if (this->server == NULL) {
			#ifdef SHOW_ERROR_MESSAGES
	    		std::cerr << "ERROR, no such host" << std::endl;
			#endif
	    	return false;
	    }

	    bzero((char *) &this->serv_addr, sizeof(this->serv_addr));
	    this->serv_addr.sin_family = AF_INET;
	    bcopy((char *)this->server->h_addr,
	    	  (char *)&this->serv_addr.sin_addr.s_addr,
	    	   this->server->h_length
	    	  );
	    this->serv_addr.sin_port = htons(this->portno);

	    if (connect(this->sockfd,(struct sockaddr *) &this->serv_addr,sizeof(this->serv_addr)) < 0){
			#ifdef SHOW_ERROR_MESSAGES
	    		std::cerr << "ERROR connecting to: " << this->address << std::endl;
			#endif
	    	return false;
	    }

		#ifdef SHOW_DEBUG_MESSAGES
			std::cout << "TCPConnection open" << std::endl;
		#endif

		return true;
}


void TCPConnection::Close(){
    close(this->sockfd);
}

void TCPConnection::setLength(int len){
	this->length = len;
}


int TCPConnection::send(std::string &data){
	return write(this->sockfd,data.c_str(),data.length());
}


int TCPConnection::send(const char * data){
	return write(this->sockfd,data,sizeof(data)-2);
}


int TCPConnection::send(int length, char * &data){
	return write(this->sockfd,data,length);
}

int TCPConnection::receive(int length, char * &data){
    bzero(data,length);
	return read(this->sockfd,data,length);
}

void TCPConnection::println(const char * line){
	int len = strlen(line);
//	char * msg = new char[len+2];
//	strcpy(msg, line);
//	msg[len] = '\r';
//	msg[len+1] = '\n';
//	msg[len+2] = '\0';
//	len = strlen(msg);
	write(this->sockfd, line, len);
	write(this->sockfd, "\r\n", 2);
//	delete msg;
}

void TCPConnection::readln(char * &line){
	char c = '\1';
	int i = 0;
	while ( (c != '\n') && (c != '\0') && (i < this->length - 1) ){

		//TODO: talvez seja bom realocar a memoria
		read(this->sockfd,&c,1);
		#ifdef SHOW_DEBUG_MESSAGES
			std::cout << c ;
		#endif

		if((c !='\r')&&(c != '\n'))
			line[i++] = c;

	}
	line[i] = '\0';
}


} /* namespace std */
