/*
 * IObserver.h
 *
 *  Created on: Feb 23, 2015
 *      Author: uis
 */

#ifndef IOBSERVER_H_
#define IOBSERVER_H_

namespace uis {

class IObserver {
public:
	IObserver();
	virtual ~IObserver(){}
	virtual void notify() = 0;
};

} /* namespace uis */

#endif /* IOBSERVER_H_ */
