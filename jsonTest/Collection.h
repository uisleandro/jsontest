#include <string>
#include <deque>
#include <unordered_map>
//#include <map>

#ifndef CONNECTION_H

#define CONNECTION_H

namespace uis {

enum CollectionItemType {
	DEQUE,
	//STRING_ARRAY,
	UNORDERED_MAP,
	CHAR_PTR
};

class CollectionFactory{

public:

	enum CollectionItemType Type;

	union{
		std::deque<CollectionFactory *>  * CollectionArray;
		std::unordered_map<std::string, CollectionFactory *> * AssociativeArray;
		char * StringValue;
	};

	CollectionFactory(CollectionItemType type);
	virtual ~ CollectionFactory();

	std::string toString();

};


}

#endif
