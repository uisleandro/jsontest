/*
 * CharacterSource.h
 *
 *  Created on: Feb 17, 2015
 *      Author: uis
 */

#ifndef CHARACTERSOURCE_H_
#define CHARACTERSOURCE_H_

#include "IObserver.h"
#include "IStringSource.h"
#include <cstring>
#include "ICharSource.h"

namespace uis {

class CharacterSource: public ICharSource {
protected:
	char * buffer;
	int bufferSize;
	int length;
	int position;

public:
	/**
	 * Decide if i will use it
	 * */
	//IObserver * observer;
	IStringSource * StringSource;

	CharacterSource(int bufferSize);
	virtual ~CharacterSource();

	//A must implement function
	//append
	//input: char*
	virtual void append(const char * &s);

	//returns true if the
	//next character is avaliable
	//output: char
	virtual bool next();
};

} /* namespace uis */

#endif /* CHARACTERSOURCE_H_ */
