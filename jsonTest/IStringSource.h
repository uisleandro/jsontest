/*
 * IStringSource.h
 *
 *  Created on: Feb 23, 2015
 *      Author: uis
 */

#ifndef ISTRINGSOURCE_H_
#define ISTRINGSOURCE_H_

namespace uis {

class IStringSource {
public:
	char * Buffer;
	//IStringSource(){}
	virtual ~IStringSource(){}
	virtual bool next() = 0;
};

} /* namespace uis */

#endif /* IOBSERVER_H_ */
