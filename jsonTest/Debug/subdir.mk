################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../CharacterSource.cpp \
../Collection.cpp \
../LexicalAnalyzer.cpp \
../MyWebApp.cpp \
../Parser.cpp \
../TCPConnection.cpp \
../WebStringSource.cpp \
../main_app.cpp 

OBJS += \
./CharacterSource.o \
./Collection.o \
./LexicalAnalyzer.o \
./MyWebApp.o \
./Parser.o \
./TCPConnection.o \
./WebStringSource.o \
./main_app.o 

CPP_DEPS += \
./CharacterSource.d \
./Collection.d \
./LexicalAnalyzer.d \
./MyWebApp.d \
./Parser.d \
./TCPConnection.d \
./WebStringSource.d \
./main_app.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


