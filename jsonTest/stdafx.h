/*
 * stdafx.h
 *
 *  Created on: Dec 1, 2014
 *      Author: uisleandro
 */

#ifndef STDAFX_H_
#define STDAFX_H_

#define SHOW_MESSAGES

//#define SHOW_COMMANDS
#define SHOW_RETURN_MESSAGES
//#define SHOW_STATUS_MESSAGES
//#define SHOW_DEBUG_MESSAGES
#define SHOW_ERROR_MESSAGES

//#define CONSOLE_CPP_
//#define DAEMON_CPP_

#include <chrono>
#include <mutex>
#include <thread>

#include <fstream>

#ifdef SHOW_MESSAGES
	#include <iostream>
#endif


#include <vector>
#include <memory>
#include <string>
#include <utility>

//#include <string.h>
#include <sstream>
#include <ctime>

#ifdef USE_MYSQL

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
//#include <iostream>

//#include "AbsTester.h"


#endif /* STDAFX_H_ */

