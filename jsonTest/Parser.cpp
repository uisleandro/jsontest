/*
 * Parser.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: uis
 */

#include "Parser.h"

namespace uis {

Parser::Parser() {
	this->webStringSource = new WebStringSource(10, "localhost", 80);
	this->characterSource = new CharacterSource(10);
	this->analex = new LexicalAnalyzer(20, 1024);

	this->characterSource->StringSource = webStringSource;
	this->analex->CharSource = this->characterSource;
}

Parser::~Parser() {
	delete this->webStringSource;
	delete this->characterSource;
	delete this->analex;
}

#ifdef DEBUG

void Parser::debug(){

	uis::Token * to = this->analex->Token;

	int t = (int)to->type;

	switch(t){
	case 0:
		std::cout << "null()";
		break;
	case 1:
		std::cout << "{";
		break;
	case 2:
		std::cout << "}";
	break;
	case 3:
		std::cout << "[";
	break;
	case 4:
		std::cout << "]";
	break;
	case 5:
		std::cout << ":";
	break;
	case 6:
		std::cout << ",";
	break;
	case 7:
		std::cout << "int(" << to->stringValue << ")";
	break;
	case 8:
		std::cout << "double(" << to->stringValue << ")";
	break;
	case 9:
		std::cout << "string(" << to->stringValue << ")";
	break;
	case 10:
		std::cout << "string(" << to->stringValue << ")";
	break;
	case 11:
		std::cout << "string(" << to->stringValue << ")";
	break;
	case 12:
		std::cout << "id(" << to->stringValue << ")";
	break;
	default:
		std::cout << "??";
	}

}

#endif

CollectionFactory * Parser::Parse(){

	this->webStringSource->Get("localhost","/hello.json");

	CollectionFactory * current = nullptr;

	if((this->analex->next() != nullptr)){
		if(this->object((CollectionFactory **)&current)){
			return current;
		}
		else if(this->array((CollectionFactory **)&current)){
			return current;
		}
		else{
			std::cerr << "%" << this->analex->Token->type;
			std::cerr << "erro0" << std::endl;
			return nullptr;
		}
	}

	//erro
	std::cerr << "erro1" << std::endl;
	return nullptr;

}

/**
 * An object is an array of attributes
 * */
bool Parser::object(CollectionFactory ** current){

	// {
	if((this->analex->Token->type != uis::TOKEN_OPEN_BRACE)||(this->analex->next() == nullptr)){
		return false;
	}
#ifdef DEBUG
	debug();
#endif

	*current = new CollectionFactory(UNORDERED_MAP);

	if(this->attribute(current)){

		while(this->analex->Token->type == uis::TOKEN_COMMA){

#ifdef DEBUG
	debug();
#endif

			//char after ,
			if(this->analex->next() == nullptr){
				std::cerr << "erro2" << std::endl;
				return false;
			}

			this->attribute(current);
			//else error. after ',' ever comes an attribute
		}
	}

	// }

#ifdef DEBUG
	debug();
#endif

	if(this->analex->Token->type == uis::TOKEN_CLOSE_BRACE){
		this->analex->next();
		return true;
	}

	std::cerr << "%" << this->analex->Token->type;
	std::cerr << "erro3" << std::endl;
	return false;
}

bool Parser::attribute(CollectionFactory ** current){

	std::string key;

	if(!this->stringKey(key)){
		std::cerr << "%" << this->analex->Token->type;
		std::cerr << "erro4" << std::endl;
		return false;
	}

#ifdef DEBUG
	debug();
#endif

	if(this->analex->Token->type != uis::TOKEN_COLON){
		std::cerr << "erro5" << std::endl;
		return false;
	}

#ifdef DEBUG
	debug();
#endif

	this->analex->next();

	CollectionFactory * thisOne = nullptr;

	if(this->stringValue(&thisOne)){
		(*current)->AssociativeArray->insert(std::pair<std::string, CollectionFactory *>(key, thisOne));
		return true;
	}
	else if(this->array(&thisOne)){
		(*current)->AssociativeArray->insert(std::pair<std::string, CollectionFactory *>(key, thisOne));
		return true;
	}
	else if(this->object(&thisOne)){
		(*current)->AssociativeArray->insert(std::pair<std::string, CollectionFactory *>(key, thisOne));
		return true;
	}

	std::cerr << "%" << this->analex->Token->type;
	std::cerr << "erro7" << std::endl;
	return false;
}


bool Parser::array(CollectionFactory ** current){

	// [
	if((this->analex->Token->type != uis::TOKEN_OPEN_BRACKET)||(this->analex->next() == nullptr)){
		return false;
	}

#ifdef DEBUG
	debug();
#endif

	*current = new CollectionFactory(DEQUE);

	CollectionFactory * child;

	if(this->array(&child) || this->object(&child) || this->stringValue(&child)){
		(*current)->CollectionArray->push_back(child);
	}


	while(this->analex->Token->type == uis::TOKEN_COMMA){
#ifdef DEBUG
	debug();
#endif

		if(this->analex->next() == nullptr){
			std::cerr << "erro8" << std::endl;
			return false;
		}

		if(this->array(&child) || this->object(&child) || this->stringValue(&child)){
			(*current)->CollectionArray->push_back(child);
		}
	}

#ifdef DEBUG
	debug();
#endif

	// ]
	if(this->analex->Token->type == uis::TOKEN_CLOSE_BRACKET){
		this->analex->next();
	    return true;
	}
	std::cerr << "160>" << this->analex->Token->type;
	std::cerr << "erro11" << std::endl;
	return false;
}

bool Parser::stringKey(std::string &current){


	if(this->analex->Token->type != uis::TOKEN_WORD){
		return false;
	}

#ifdef DEBUG
	debug();
#endif

	current = this->analex->Token->stringValue;

	this->analex->next();
	return true;
}


bool Parser::stringValue(CollectionFactory ** current){

	if(this->analex->Token->type != uis::TOKEN_WORD){
		return false;
	}


#ifdef DEBUG
	debug();
#endif

	*current = new CollectionFactory(CHAR_PTR);
	(*current)->StringValue = this->analex->Token->stringValue;

	this->analex->next();
	return true;
}


} /* namespace uis */
