/*
 * WebStringSource.h
 *
 *  Created on: Feb 24, 2015
 *      Author: uis
 */

#ifndef WEBSTRINGSOURCE_H_
#define WEBSTRINGSOURCE_H_

#include "TCPConnection.h"
#include "IStringSource.h"

#define EQUALS(A,B) (strcmp((char *)A,(char *)B) == 0)

namespace uis {

class WebStringSource: public IStringSource {

protected:
	int bufferSize;
	TCPConnection * conn;
	int thisPart;
	int parts;
	int left;
	char * header;

public:
	WebStringSource(int bufferSize, const char * ip, int portno);
	virtual bool Get(const char * host, const char * path);
	virtual bool next();
	virtual ~WebStringSource();
};

} /* namespace uis */

#endif /* WEBSTRINGSOURCE_H_ */
